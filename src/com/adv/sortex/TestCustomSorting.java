package com.adv.sortex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestCustomSorting {

	public static void main(String[] args) {
		List<EmpObj> empList = new ArrayList<>();
		empList.add(new EmpObj("tiger", 10, "tiger@gmail.com"));
		empList.add(new EmpObj("lion", 11, "lion@gmail.com"));
		empList.add(new EmpObj("lion", 10, "lion@gmail.com"));
		empList.add(new EmpObj("zebra", 12, "zebra@gmail.com"));
		empList.add(new EmpObj("cat", 9, "cat@gmail.com"));
		
		System.out.println("emplist before sorting by Name--"+empList);
		Collections.sort(empList, new NameSorting());
		System.out.println("emplist after sorting--"+empList);
		
		System.out.println("------------------------------------");
		System.out.println("emplist before sorting by Age--"+empList);
		Collections.sort(empList, new AgeSorting());
		System.out.println("emplist after sorting--"+empList);
		
		System.out.println("------------------------------------");
		System.out.println("emplist before sorting by Email--"+empList);
		Collections.sort(empList, new EmailSorting());
		System.out.println("emplist after sorting--"+empList);
	};
}
