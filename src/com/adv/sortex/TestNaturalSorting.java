package com.adv.sortex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestNaturalSorting {

	public static void main(String[] args) {
		List<StaffObj> staffList = new ArrayList<>();
		staffList.add(new StaffObj("tiger", 10, "tiger@gmail.com"));
		staffList.add(new StaffObj("lion", 11, "lion@gmail.com"));
		staffList.add(new StaffObj("lion", 10, "lion@gmail.com"));
		staffList.add(new StaffObj("zebra", 12, "zebra@gmail.com"));
		staffList.add(new StaffObj("cat", 9, "cat@gmail.com"));
		
		System.out.println("emplist before sorting--"+staffList);
		Collections.sort(staffList);
		System.out.println("emplist after sorting--"+staffList);
	};
}
