package com.adv.sortex;

import java.util.Comparator;

public class NameSorting implements Comparator<EmpObj> {

	@Override
	public int compare(EmpObj emp1, EmpObj emp2) {
		return emp1.getName().compareTo(emp2.getEmail());
	}
}
