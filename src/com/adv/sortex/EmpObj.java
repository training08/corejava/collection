package com.adv.sortex;

public class EmpObj /* implements Comparable<EmpObj> */ {

	private String name;

	private int age;

	private String email;

	public EmpObj(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Emp [name=" + name + ", age=" + age + ", email=" + email + "]";
	}

	/*
	 * @Override public int compareTo(EmpObj emp) { return this.getAge() -
	 * emp.getAge(); }
	 */
}
