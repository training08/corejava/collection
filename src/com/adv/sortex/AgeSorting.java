package com.adv.sortex;

import java.util.Comparator;

public class AgeSorting implements Comparator<EmpObj> {

	@Override
	public int compare(EmpObj emp1, EmpObj emp2) {
		return emp2.getAge() - emp1.getAge();
	}
}
