package com.adv.collectionex;

public class TestEmpList {

	public static void main(String[] args) {
		
		EmpList empList = new EmpList();
		
		Emp e1 = new Emp(1, "tiger", 5, "t@gmail.com");
		Emp e2 = new Emp(2, "lion", 8, "l@gmail.com");
		//Emp e3 = new Emp(1, "tiger", 5, "t@gmail.com");
		Emp e3 = new Emp(4, "zebra", 10, "z@gmail.com");
		Emp e4 = new Emp(3, "cat", 3, "c@gmail.com");
		
		empList.add(e1);
		empList.add(e2);
		empList.add(e1);
		empList.add(e3);
		empList.add(e4);
		
		empList.print();
		
		System.out.println("No of Objects -----> "+empList.size());
		
		empList.delete(e3);
		
		empList.print();
		System.out.println("No of Objects -----> "+empList.size());
	}
}
