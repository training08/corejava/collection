package com.adv.collectionex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test1 {

	public static void main(String[] args) {
		List<String> strList1 = new ArrayList<>();
		strList1.add("A");
		strList1.add("B");
		strList1.add("C");
		strList1.add("D");

		List<String> strList2 = new ArrayList<>();
		strList2.add("E");
		strList2.add("F");
		strList2.add("G");
		strList2.add("H");

		List<List<String>> combinedList = Arrays.asList(strList1, strList2);

		System.out.println("combinedList------>" + combinedList);

		List<String> strListCombined = combinedList.stream().flatMap(val -> val.stream()).collect(Collectors.toList());
		System.out.println("strListCombined------>" + strListCombined);

		class Employee {
			private String name;
			private int age;
			private String email;

			public Employee(String name, int age, String email) {
				this.name = name;
				this.age = age;
				this.email = email;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public int getAge() {
				return age;
			}

			public void setAge(int age) {
				this.age = age;
			}

			public String getEmail() {
				return email;
			}

			public void setEmail(String email) {
				this.email = email;
			}
		}

		List<Employee> empList = new ArrayList<>();
		empList.add(new Employee("one", 1, "one@g.com"));
		empList.add(new Employee("two", 2, "two@g.com"));
		empList.add(new Employee("three", 3, "three@g.com"));
		empList.add(new Employee("four", 4, "four@g.com"));

		List<String> empStrList = empList.stream().map(e -> e.getName()).collect(Collectors.toList());
		System.out.println("empStrList=" + empStrList);

		List<String> list = Arrays.asList("5.6", "7.4", "4", "1", "2.3");
		
		System.out.println("list------->"+list);
		
		list.stream().flatMap(num -> Stream.of(num)).forEach(System.out::println);
		
		List<String> nameList = new ArrayList<>();
		nameList.add("Sample");
		nameList.add("Training");
		nameList.add("Brother");
		nameList.add("Sister");
		
		nameList.stream().map(e -> e.substring(2, 4)).collect(Collectors.toList()).forEach(System.out::println);
	}
}
