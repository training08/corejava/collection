package com.adv.collectionex;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Test2 {

	public static void main(String[] args) {
		int arr[] = {3,2,10,18,12,8,6,5,7,8,7,18};
		Arrays.stream(arr).filter(e -> e%2==0).forEach(System.out::println);
		List<Integer> intList = Arrays.stream(arr).boxed().filter(e -> e%2==0).collect(Collectors.toList());
		System.out.println(intList);
		List<Integer> intList1 = Arrays.stream(arr).boxed().collect(Collectors.toList());
		System.out.println(intList1);
		List<Integer> intList2 = Arrays.stream(arr).boxed().sorted().collect(Collectors.toList());
		System.out.println(intList2);
		long countVal = Arrays.stream(arr).boxed().sorted().count();
		System.out.println(countVal);
		List<Integer> distinctList = Arrays.stream(arr).boxed().sorted().distinct().collect(Collectors.toList());
		System.out.println(distinctList);
		long maxVal = Arrays.stream(arr).boxed().max(Integer::compare).get();
		System.out.println(maxVal);
		long minVal = Arrays.stream(arr).boxed().min(Integer::compare).get();
		System.out.println(minVal);
	}
}
