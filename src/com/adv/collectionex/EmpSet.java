package com.adv.collectionex;

import java.util.HashSet;
import java.util.Set;

public class EmpSet {

	private Set<Emp> empSet = new HashSet<>();
	
	public void add(Emp emp) {
		empSet.add(emp);
	}
	
	public void delete(Emp emp) {
		empSet.remove(emp);
	}
	
	public void print() {
		System.out.println("Emp Set="+empSet);
		System.out.println("-----------------------------------");
	}
	
	public int size() {
		return empSet.size();
	}
}
