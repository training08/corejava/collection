package com.adv.collectionex;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetEx {
	
	private Set<String> strSet = new HashSet<>();

	public void add(String inputStr) {
		strSet.add(inputStr);
	}
	
	public void delete(String inputStr) {
		strSet.remove(inputStr);
	}
	
	public void iterate() {
		Iterator<String> iter = strSet.iterator();
		while (iter.hasNext()) {
			String strVal = iter.next();
			System.out.println("strVal="+strVal);
		}
	}
	
	public static void main(String[] args) {
		SetEx setEx = new SetEx();
		setEx.add("tiger");
		setEx.add("lion");
		setEx.add("tiger");
		setEx.add("lion");
		setEx.add("tiger");
		setEx.add("zebra");
		
		setEx.iterate();
		
		setEx.delete("tiger");
		System.out.println("----------------------");
		setEx.iterate();
	}
}
