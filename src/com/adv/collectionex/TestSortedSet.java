package com.adv.collectionex;

import java.util.SortedSet;
import java.util.TreeSet;

public class TestSortedSet {

	public static void main(String[] args) {
		SortedSet<String> ss = new TreeSet<>();
		ss.add("tiger");
		ss.add("lion");
		ss.add("zebra");
		//ss.add(null);
		ss.add("cow");
		ss.add("cat");
		ss.add("tiger");
		ss.add("elephant");
		ss.add("lion");
		
		System.out.println("sortedset value="+ss);
	}
}
