package com.adv.collectionex;

import java.util.ArrayList;
import java.util.List;

public class EmpList {

	private List<Emp> empList = new ArrayList<>();
	
	public void add(Emp emp) {
		empList.add(emp);
	}
	
	public void delete(Emp emp) {
		empList.remove(emp);
	}
	
	public void print() {
		System.out.println("Emp List="+empList);
		System.out.println("-----------------------------------");
	}
	
	public int size() {
		return empList.size();
	}
}
