package com.adv.collectionex;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import com.adv.sortex.EmpObj;
import com.adv.sortex.NameSorting;

public class TestSortedSetWithCustomObj {

	public static void main(String[] args) {
//		Student s1 = new Student(1, "tiger", 10, "tiger@g", "ramesh", new Date(), "ramesh", new Date());
//		Student s2 = new Student(2, "lion", 12, "lion@g", "ramesh", new Date(), "ramesh", new Date());
//		Student s3 = new Student(3, "zebra", 12, "zebra@g", "ramesh", new Date(), "ramesh", new Date());
//		Student s4 = new Student(4, "cat", 9, "cat@g", "ramesh", new Date(), "ramesh", new Date());
//		Student s5 = new Student(5, "lion", 12, "lion@g", "ramesh", new Date(), "ramesh", new Date());
//		Student s6 = new Student(6, "zebra", 7, "zebra@g", "ramesh", new Date(), "ramesh", new Date());
//		Student s7 = new Student(7, "tiger", 3, "tiger@g", "ramesh", new Date(), "ramesh", new Date());
		
		EmpObj s1 = new EmpObj("tiger", 10, "tiger@gmail.com");
		EmpObj s2 = new EmpObj("lion", 11, "lion@gmail.com");
		EmpObj s3 = new EmpObj("lion", 10, "lion@gmail.com");
		EmpObj s4 = new EmpObj("zebra", 12, "zebra@gmail.com");
		EmpObj s5 = new EmpObj("cat", 9, "cat@gmail.com");
		
		SortedSet<EmpObj> ss = new TreeSet<>(new NameSorting());
		
		SortedSetWithCustomObj<EmpObj> sortedSetWithCustomObj = new SortedSetWithCustomObj<>(new TreeSet<>(new NameSorting()));
		sortedSetWithCustomObj.add(s1);
		sortedSetWithCustomObj.add(s2);
		sortedSetWithCustomObj.add(s3);
		sortedSetWithCustomObj.add(s4);
		sortedSetWithCustomObj.add(s5);
//		sortedSetWithCustomObj.add(s6);
//		sortedSetWithCustomObj.add(s7);
		
		sortedSetWithCustomObj.iterate();
	}
}
