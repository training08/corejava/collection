package com.adv.collectionex;

public class TestEmpSet {

	public static void main(String[] args) {

		EmpSet empSet = new EmpSet();

		Emp e1 = new Emp(1, "tiger", 5, "tiger@gmail.com");
		Emp e2 = new Emp(2, "lion", 8, "lion@gmail.com");
		Emp e3 = new Emp(1, "tiger1", 5, "tiger@gmail.com");
		//Emp e3 = new Emp(1, "cow", 6, "cow@gmail.com");
		Emp e4 = new Emp(4, "zebra", 10, "zebra@gmail.com");
		Emp e5 = new Emp(3, "cat", 3, "cat@gmail.com");

		empSet.add(e1);
		empSet.add(e2);
		empSet.add(e1);
		empSet.add(e3);
		empSet.add(e4);
		empSet.add(e5);

		empSet.print();

		System.out.println("No of Objects -----> " + empSet.size());

		empSet.delete(e3);

		empSet.print();
		System.out.println("No of Objects -----> " + empSet.size());
	}
}
