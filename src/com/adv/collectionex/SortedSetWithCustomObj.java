package com.adv.collectionex;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

public class SortedSetWithCustomObj<T> {

	//private SortedSet<Student> sortedSet = new TreeSet<>();
	
	//private SortedSet<T> sortedSet = new TreeSet<T>(new NameSorting());
	
	private SortedSet<T> sortedSet;
	
	public SortedSetWithCustomObj() {
		this.sortedSet = new TreeSet<>();;
	}
	
	public SortedSetWithCustomObj(SortedSet<T> sortedSet) {
		this.sortedSet = sortedSet;
	}
	
	public void add(T t) {
		sortedSet.add(t);
	}
	
	public void delete(T t) {
		sortedSet.remove(t);
	}
	
	public void iterate() {
		Iterator<T> studIter = sortedSet.iterator();
		while (studIter.hasNext()) {
			T t = studIter.next();
			System.out.println("student----->"+t);
		}
	}
}
