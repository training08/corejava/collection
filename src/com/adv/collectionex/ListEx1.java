package com.adv.collectionex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListEx1 {

	public static void main(String[] args) {
		List<String> strList = new ArrayList<>();
		strList.add("tiger");
		strList.add("lion");
		strList.add("zebra");
		strList.add("elephant");
		
		System.out.println(strList);
		
		strList.remove("tiger");
		
		System.out.println(strList);
		System.out.println("------------------------------");
		//iteration - using for loop
		for (int i = 0; i < strList.size(); i++) {
			String strValue = strList.get(i);
			System.out.println("Str Value="+strValue);
		}
		System.out.println("------------------------------");
		//iteration - using itertor
		Iterator<String> iter = strList.iterator();
		while (iter.hasNext()) {
			String str = iter.next();
			System.out.println("str iter="+str);
		}
		System.out.println("------------------------------");
		ListIterator<String> strListIter = strList.listIterator();
		while (strListIter.hasNext()) {
			String str = strListIter.next();
			System.out.println("str list iter="+str);
		}
		System.out.println("------------------------------");
		//for each
		for (String strVal : strList) {
			System.out.println("str foreach ="+strVal);
		}
	}
}
